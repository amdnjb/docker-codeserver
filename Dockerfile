FROM linuxserver/code-server:version-v3.11.0

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    git \
    zip \
    nano \
    wget \
    tree \
    gnupg \
    rsync \
    unzip \
    unrar \
    telnet \
    screen \
    lsb-release \
    iputils-ping \
    bash-completion \
    ca-certificates \
    apt-transport-https \
    && rm -rf /var/lib/apt/lists/*

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && sudo ./aws/install \
    && rm -rf awscliv2.zip aws

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
    && echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
    && apt-get update && apt-get install -y \
    docker-ce \
    docker-ce-cli \
    docker-compose \
    containerd.io \
    && rm -rf /var/lib/apt/lists/* \
    && usermod -aG docker abc
